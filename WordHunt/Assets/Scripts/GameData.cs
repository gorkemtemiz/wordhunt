﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : Singleton<GameData>
{
    public string gameIP;
    public string gamePort;
    public int gameFieldX;
    public int gameFieldY;
    public int unableTilesAmount;
    public int doublePointTilesAmount;
    public int triplePointTilesAmount;
    public int pointToGetRound;
    public int roundToWin;
    public List<PlayerData> playerList;
    public List<int> unableTileList;
    public List<int> doublePointTileList;
    public List<int> triplePointTileList;
}
