﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class GameManager : Singleton<GameManager>
{
    #region Public Objects For Editor

    [Header("UI Pages Area")]
    public GameObject firstPanel;
    public GameObject inGamePanel;

    [Header ("Create Game Area")]
    public GameObject create_PlayerNicknameInput;
    public GameObject create_GameFieldXInput;
    public GameObject create_GameFieldYInput;
    public GameObject create_UnableTileInput;
    public GameObject create_DoublePointTileInput;
    public GameObject create_TriplePointTileInput;
    public GameObject create_PointToGetRoundInput;
    public GameObject create_RoundToWinInput;

    [Header ("Join Game Area")]
    public GameObject iPInput;
    public GameObject portInput;
    public GameObject nicknameInput;

    [Header ("First Page Buttons Area")]
    public GameObject createGame_Button;
    public GameObject joinGame_Button;

    [Header ("Game Info Area")]
    public GameObject info_PlayerNicknameText;
    public GameObject info_GameFieldText;
    public GameObject info_UnableTileText;
    public GameObject info_DoublePointTileText;
    public GameObject info_TriplePointTileText;
    public GameObject info_PointToGetRoundText;
    public GameObject info_RoundToWinText;

    [Header ("Player List Area")]
    public GameObject playerListText;

    [Header ("Word Block Area")]
    public GameObject button_Normal;
    public GameObject button_Unable;
    public GameObject button_Double;
    public GameObject button_Triple;
    public GameObject wordBlockParent;

    [Header ("In-Game Page Buttons Area")]
    public GameObject leftToRightButton;
    public GameObject topToBottomButton;
    public GameObject newWordInput;

    #endregion

    #region Private & Hidden Objects For Scripting

    [HideInInspector] public List<GameObject> wordBlocksList_Linear;
    [HideInInspector] public List<GameObject> wordBlocksList_Part;
    [HideInInspector] public List<List<GameObject>> wordBlocksList_Nested;
    [HideInInspector] public GameObject currentWordBlock;
    [HideInInspector] public GameObject lastClickedButton;
    [HideInInspector] public string pathOfDictionary;
    #endregion


    private void Start()
    {
        pathOfDictionary = "Assets/Resources/dictionary.txt";
    }

    void Update()
    {
        
    }

    public void SetField()
    {
        int _x = int.Parse(create_GameFieldXInput.GetComponent<TMPro.TMP_InputField>().text);
        int _y = int.Parse(create_GameFieldYInput.GetComponent<TMPro.TMP_InputField>().text);

        GameData.Instance.gameFieldX = _x;
        GameData.Instance.gameFieldY = _y;
        SetGameData("000.000.00.000", "6969"); /// port ve ip burada değişken olarak verilecek
        GenerateWordBlocks(_x, _y);
        ChangePages(firstPanel, inGamePanel);
    }

    public void SetGameData(string _ip, string _port)
    {
        GameData.Instance.gameIP = _ip;
        GameData.Instance.gamePort = _port;
        GameData.Instance.pointToGetRound = int.Parse(create_PointToGetRoundInput.GetComponent<TMPro.TMP_InputField>().text);
        GameData.Instance.roundToWin = int.Parse(create_RoundToWinInput.GetComponent<TMPro.TMP_InputField>().text);
        GameData.Instance.unableTilesAmount = int.Parse(create_UnableTileInput.GetComponent<TMPro.TMP_InputField>().text);
        GameData.Instance.doublePointTilesAmount = int.Parse(create_DoublePointTileInput.GetComponent<TMPro.TMP_InputField>().text);
        GameData.Instance.triplePointTilesAmount = int.Parse(create_TriplePointTileInput.GetComponent<TMPro.TMP_InputField>().text);
        GameData.Instance.playerList = new List<PlayerData>();
        AddPlayer();

        info_PlayerNicknameText.GetComponent<TextMeshProUGUI>().SetText("Kullanıcı Adı : " + create_PlayerNicknameInput.GetComponent<TMP_InputField>().text);
        info_GameFieldText.GetComponent<TextMeshProUGUI>().SetText("Oyun Alanı : " + GameData.Instance.gameFieldX.ToString() + " x " + GameData.Instance.gameFieldY.ToString());
        info_UnableTileText.GetComponent<TextMeshProUGUI>().SetText("Yasaklı Kare Sayısı : " + GameData.Instance.unableTilesAmount.ToString());
        info_DoublePointTileText.GetComponent<TextMeshProUGUI>().SetText("2x Puanlı Kare Sayısı : " + GameData.Instance.doublePointTilesAmount.ToString());
        info_TriplePointTileText.GetComponent<TextMeshProUGUI>().SetText("3x Puanlı Kare Sayısı : " + GameData.Instance.triplePointTilesAmount.ToString());
        info_PointToGetRoundText.GetComponent<TextMeshProUGUI>().SetText("Raundu Kazanma Puanı : " + GameData.Instance.pointToGetRound.ToString());
        info_RoundToWinText.GetComponent<TextMeshProUGUI>().SetText("Maksimum Raund Sayısı : " + GameData.Instance.roundToWin.ToString());

        PlayerData _playerData = GameData.Instance.playerList[0];
        playerListText.GetComponent<TextMeshProUGUI>().SetText(_playerData.playerName + " -> Raund : " + _playerData.playerRound + " | Puan : " + _playerData.playerScore + " <br>");
    }

    public void AddPlayer()
    {
        PlayerData newPlayerData = new PlayerData
        {
            playerName = create_PlayerNicknameInput.GetComponent<TMP_InputField>().text,
            playerScore = 0,
            playerRound = 0
        };
        GameData.Instance.playerList.Add(newPlayerData);
    }

    public void GenerateWordBlocks(int gridX, int gridY)
    {
        int t = gridX * gridY;
        int rand;
        wordBlocksList_Linear = new List<GameObject>();
        wordBlocksList_Nested = new List<List<GameObject>>();
        wordBlocksList_Part = new List<GameObject>();

        for (int i = 0; i < gridX; i++)
        {
            for(int j = 0; j < gridY; j++)
            {          
                currentWordBlock = Instantiate(button_Normal, Vector3.zero, Quaternion.identity, wordBlockParent.transform);
                currentWordBlock.GetComponent<RectTransform>().anchoredPosition = new Vector3(-(gridX*16) + (i * 32), -(gridY * 16) + (j * 32), 0);
                wordBlocksList_Linear.Add(currentWordBlock);
            }
        }

        int unTileCount = GameData.Instance.unableTilesAmount;
        List<int> unTileList = new List<int>();
        for(int a = 0; a < unTileCount; a++)
        {
            while(unTileList.Count < unTileCount)
            {
                rand = Random.Range(0, t);
                if (!unTileList.Contains(rand))
                {
                    RectTransform rect = wordBlocksList_Linear[rand].GetComponent<RectTransform>();
                    currentWordBlock = Instantiate(button_Unable, Vector3.zero, Quaternion.identity, wordBlockParent.transform);
                    currentWordBlock.GetComponent<RectTransform>().anchoredPosition3D = rect.anchoredPosition3D;
                    wordBlocksList_Linear[rand] = currentWordBlock;
                    unTileList.Add(rand);
                }
            }
        }
        GameData.Instance.unableTileList = unTileList;


        int doTileCount = GameData.Instance.doublePointTilesAmount;
        List<int> doTileList = new List<int>();
        for (int a = 0; a < doTileCount; a++)
        {
            while(doTileList.Count < doTileCount)
            {
                rand = Random.Range(0, t);
                if (!unTileList.Contains(rand) && !doTileList.Contains(rand))
                {
                    RectTransform rect = wordBlocksList_Linear[rand].GetComponent<RectTransform>();
                    currentWordBlock = Instantiate(button_Double, Vector3.zero, Quaternion.identity, wordBlockParent.transform);
                    currentWordBlock.GetComponent<RectTransform>().anchoredPosition3D = rect.anchoredPosition3D;
                    wordBlocksList_Linear[rand] = currentWordBlock;
                    doTileList.Add(rand);
                }
            }
        }
        GameData.Instance.doublePointTileList = doTileList;


        int triTileCount = GameData.Instance.triplePointTilesAmount;
        List<int> triTileList = new List<int>();
        for (int a = 0; a < triTileCount; a++)
        {
            while(triTileList.Count < triTileCount)
            {
                rand = Random.Range(0, t);
                if (!unTileList.Contains(rand) && !doTileList.Contains(rand) && !triTileList.Contains(rand))
                {
                    RectTransform rect = wordBlocksList_Linear[rand].GetComponent<RectTransform>();
                    currentWordBlock = Instantiate(button_Triple, Vector3.zero, Quaternion.identity, wordBlockParent.transform);
                    currentWordBlock.GetComponent<RectTransform>().anchoredPosition3D = rect.anchoredPosition3D;
                    wordBlocksList_Linear[rand] = currentWordBlock;
                    triTileList.Add(rand);
                }
            }
        }
        GameData.Instance.triplePointTileList = triTileList;
        
        int index = 0;
        for (int a = 0; a < gridX; a++)
        {
            wordBlocksList_Part.Clear();
            for(int b = 0; b < gridY; b++)
            {
                if(index < wordBlocksList_Linear.Count)
                {
                    wordBlocksList_Part.Add(wordBlocksList_Linear[index]);
                    index++;
                }
            }           
            wordBlocksList_Nested.Add(wordBlocksList_Part);
        }

    }

    public void PickAButton()
    {
        lastClickedButton = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;
    }

    public void WriteWord_LeftToRight()
    {
        int index = wordBlocksList_Linear.FindIndex(o => o == lastClickedButton);
        Vector2 tileIndex = new Vector2(index % GameData.Instance.gameFieldX, index / GameData.Instance.gameFieldX);
        char[] newWord = newWordInput.GetComponent<TMP_InputField>().text.ToCharArray();
        int wordPoint = 0;

        if(tileIndex.y + newWord.Length <= GameData.Instance.gameFieldY && SearchTheWord(newWordInput.GetComponent<TMP_InputField>().text))
        {
            GameObject go;
            wordBlocksList_Part.Clear();
            for (int i = 0; i < newWord.Length; i++)
            {
                go = wordBlocksList_Linear[index + (i * GameData.Instance.gameFieldX)];
                if (go.name.Contains("Button_Unable"))
                {
                    Debug.Log("Kullanılamayan kare olduğu için bu işlemi yapamazsınız");
                    break;
                }
                else if (go.name.Contains("Button_Normal") && go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text == "")
                {
                    wordPoint += 1;
                    go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText(newWord[i].ToString());
                }
                else if (go.name.Contains("Button_Double") && go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text == "")
                {
                    wordPoint += 2;
                    go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText(newWord[i].ToString());
                }
                else if (go.name.Contains("Button_Triple") && go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text == "")
                {
                    wordPoint += 3;
                    go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText(newWord[i].ToString());
                }
                else
                    Debug.Log("Dolu kare olduğu için bu işlemi yapamazsınız");
            }
        }

        Debug.Log(wordPoint);

        Debug.Log(index + " = " + tileIndex);
    }

    public void WriteWord_TopToBottom()
    {
        int index = wordBlocksList_Linear.FindIndex(o => o == lastClickedButton);
        Vector2 tileIndex = new Vector2(index % GameData.Instance.gameFieldX, index / GameData.Instance.gameFieldX);
        char[] newWord = newWordInput.GetComponent<TMP_InputField>().text.ToCharArray();
        int wordPoint = 0;

        if (tileIndex.x - newWord.Length >= 0 && SearchTheWord(newWordInput.GetComponent<TMP_InputField>().text))
        {
            GameObject go;
            wordBlocksList_Part.Clear();
            for (int i = 0; i < newWord.Length; i++)
            {
                go = wordBlocksList_Linear[index - i];
                if (go.name.Contains("Button_Unable"))
                {
                    Debug.Log("Kullanılamayan kare olduğu için bu işlemi yapamazsınız");
                    break;
                }
                else if (go.name.Contains("Button_Normal") && go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text == "")
                {
                    wordPoint += 1;
                    go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText(newWord[i].ToString());
                }
                else if (go.name.Contains("Button_Double") && go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text == "")
                {
                    wordPoint += 2;
                    go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText(newWord[i].ToString());
                }
                else if (go.name.Contains("Button_Triple") && go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text == "")
                {
                    wordPoint += 3;
                    go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText(newWord[i].ToString());
                }
                else
                    Debug.Log("Dolu kare olduğu için bu işlemi yapamazsınız");
            }
        }

        Debug.Log(wordPoint);

        Debug.Log(index + " = " + tileIndex);
    }

    public bool SearchTheWord(string word)
    {
        StreamReader streamReader = new StreamReader(pathOfDictionary);
        string line;
        bool result = false;
        while(streamReader.ReadLine() != null)
        {
            line = streamReader.ReadLine();
            if (line == word)
            {
                result = true;
                return result;
            }
            else
                continue;
        }
        streamReader.Close();
        return result;
    }

    public void ChangePages(GameObject current, GameObject next)
    {
        next.SetActive(true);
        current.SetActive(false);
    }
}
